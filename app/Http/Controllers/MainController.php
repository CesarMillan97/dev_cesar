<?php

namespace App\Http\Controllers;


use App\Models\Contact;
use Illuminate\Http\Request;
use GrahamCampbell\ResultType\Result;

class MainController extends Controller
{
    public function index (){
        return view('Frontend.index');
    }

    public function store (Request $request)
    {
        $this->validate($request, [
            'name'=>'required',
            'email'=>'required',
            'subject'=>'required',
            'message'=>'nullable',
        ],[
            'name.required'=>'Favor de llenar el campo',
            'email.required'=>'Favor de llenar el campo',
            'subject.required'=>'Favor de llenar el campo',
        ]);
        
        
        Contact::create([
            'name'=> $request->name,
            'email'=>$request->email,
            'subject'=>$request->email,
            'message'=>$request->message,
        ]);
    }

}
